"use strict";
let userNumber;
do {
    userNumber = prompt("Enter number: ");
} while (!Number.isInteger(+userNumber));
for (let number = 0; number <= userNumber; number += 5) {
    if (userNumber < 5 || userNumber === 0 || userNumber.trim() === "") {
        console.log("Sorry, no numbers");
    }
    else {
        console.log(number);
    }
}
let m = prompt("Enter first number: ");
let n = prompt("Enter second number: ");
while (m >= n || !Number.isInteger(+m) || !Number.isInteger(+n) || m.trim() ===  "" || n.trim() === "") {
    alert("Error. Try again");
    m = prompt("Enter first number: ");
    n = prompt("Enter second number: ");
}
for(let primeNumber = m; primeNumber <= n; primeNumber++){
    if(primeCheck(primeNumber)){
        console.log(primeNumber);
    }
}
function primeCheck (number) {
    for(let i = 2; i < number; i++){
        if(number % i === 0){
            return false;
        } 
    }
    return number > 1; 
}